# Terraria Oldgen Reader
Load and convert save files for the old Terraria mobile and 3DS versions.

# Progress
* Oldgen Players 100%
* Oldgen Worlds   70%
* 3DS Players     50%
* 3DS Worlds       0%
