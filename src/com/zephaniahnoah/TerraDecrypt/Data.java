package com.zephaniahnoah.TerraDecrypt;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Data implements Log {
	public static Map<Integer, String> prefixes = new HashMap<>();
	public static Map<Integer, String> buffs = new HashMap<>();
	public static Map<Integer, String> tiles = new HashMap<>();
	private static Data instance;

	public static void init() {
		instance = new Data();
		instance.fill(prefixes, "/data/Prefixes.txt");
		instance.fill(buffs, "/data/Buffs.txt");
		instance.fill(tiles, "/data/Tiles.txt");
	}

	private void fill(Map<Integer, String> map, String file) {
		log("loading from " + file);
		InputStream in = Main.class.getResourceAsStream(file);
		BufferedReader r = new BufferedReader(new InputStreamReader(in));
		String line = "";
		try {
			while ((line = r.readLine()) != null) {
				String[] split = line.split("=");
				map.put(Integer.parseInt(split[0]), split[1]);
			}
		} catch (Exception e) {
			log(e.getMessage() + " at line " + line);
			e.printStackTrace();
		}
	}
}
