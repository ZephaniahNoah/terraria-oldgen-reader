package com.zephaniahnoah.TerraDecrypt;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import com.zephaniahnoah.TerraDecrypt.Tile.LiquidType;

public class Imager implements Log {

	private static int sky = new Color(116, 191, 255).getRGB();

	private static int underground = new Color(87, 57, 20).getRGB();
	private static int cavern = new Color(76, 68, 60).getRGB();
	private static int hell = new Color(165, 45, 8).getRGB();

	private static int water = new Color(0, 90, 255).getRGB();
	private static int lava = new Color(215, 39, 6).getRGB();
	private static int honey = new Color(230, 178, 8).getRGB();

	private static int width;
	private static int height;

	static Imager instance;

	public static void WorldToImage(World w) {
		width = w.width;
		height = w.height;

		instance = new Imager();

		instance.imageIt(w, "tiles");
	}

	private static Map<Integer, Integer> walls = new HashMap<Integer, Integer>();

	private void imageIt(World world, String name) {
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {

				Tile tile = world.tiles[x][y];

				int color = TileType.getColorFromID(tile.type).getRGB();

				if (!tile.isActive) {
					color = sky;
					if (y > world.groundLevel) {
						color = underground;
					}
					if (y > world.rockLevel) {
						color = cavern;
					}
					if (y > world.hellLevel) {
						color = hell;
					}

					int wallID = tile.wall;

					if (wallID < 0) {
						wallID += 256;
					}

					if (wallID != 0) {
						color = Wall.values()[wallID].color.getRGB();
					}

					if (walls.containsKey(wallID)) {
						int num = walls.get(wallID);
						num++;
						walls.put(wallID, num);
					} else {
						walls.put(wallID, 1);
					}

//					if (tile.wall - 128 == Wall.BlueDungeon.ordinal()) {
//						color = Color.BLACK.getRGB();
//					}

					if (tile.liquidType != LiquidType.None) {
						if (tile.liquidType == LiquidType.Water) {
							color = water;
						}

						if (tile.liquidType == LiquidType.Lava) {
							color = lava;
						}

						if (tile.liquidType == LiquidType.Honey) {
							color = honey;
						}
					}
				}

				bi.setRGB(x, y, color);
			}
		}

		for (Entry<Integer, Integer> ent : walls.entrySet()) {
			log("There are " + ent.getValue() + " of " + Wall.values()[ent.getKey()]);
		}

		// Save as PNG
		File file = new File(name + ".png");
		try {
			ImageIO.write(bi, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
