package com.zephaniahnoah.TerraDecrypt;

public class Item {
	public int id, prefix, count;
	
	public Item(int id) {
		this(id, 1, 0);
	}
	
	public Item(int id, int count) {
		this(id, count, 0);
	}
	
	public Item(int id, int count, int prefix) {
		this.id = id;
		this.prefix = prefix;
		this.count = count;
	}
}
