package com.zephaniahnoah.TerraDecrypt;

import java.util.HashMap;
import java.util.Map;

public class ItemTypes {

	private static Map<String, ItemIDs> items = new HashMap<>();

	private static class ItemIDs {
		public int[] ids = new int[Edition.values().length];

		public ItemIDs(int id, Edition ed) {
			ids[ed.ordinal()] = id;
		}
	}

	public static void addItem(String key, int i, Edition ed) {
		if (items.containsKey(key))
			items.get(key).ids[ed.ordinal()] = i;
		else
			items.put(key, new ItemIDs(i, ed));
	}

	public static String getNameFromID(int id, Edition ed) {
		for (String s : items.keySet())
			if (items.get(s).ids[ed.ordinal()] == id)
				return s;
		return "ID:[" + id + "]";
	}

	public static int getIDFromName(String name, Edition ed) {
		if (items.containsKey(name))
			return items.get(name).ids[ed.ordinal()];
		return 0;
	}
}