package com.zephaniahnoah.TerraDecrypt;

public interface Log {

	public default void log() {
		log("");
	}

	public default void log(Object o) {
		System.out.println("[" + getClass().getSimpleName() + "] " + o);
	}
}
