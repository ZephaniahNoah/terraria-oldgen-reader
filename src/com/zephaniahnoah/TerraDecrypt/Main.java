package com.zephaniahnoah.TerraDecrypt;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.zip.Inflater;

import MarcusD.TerraInvedit.encryption.Blowfish;
import github.MichaelBeeu.util.EndianDataInputStream;

public class Main implements Log {

	private static final String key = "9ae3efe6a4debdeb8290eabd5f832ad4";
	public static final String worldKey = "0d710d1d63ff36fe9bafd4c3874c64dd";
	public static Main instance;
	private static boolean writeFile = false;

	public static void main(String[] args) {
		instance = new Main(args);
	}

	public Main(String[] args) {
		log("Loading items...");
		loadItems();
		log("Done loading items.");
		log("Loading other data...");

		Data.init();

		log("Done loading all data.");

		if (args.length > 0) {
			String file = args[0];
			File f = new File(file);
			log("Reading " + f.getPath());
			if (f.exists()) {
				Blowfish bl;

				byte[] data;
				try {
					data = Util.getBytesFromFile(f);

					if (file.endsWith(".csworld")) {

						Decoder decoder = Base64.getMimeDecoder();

						data = decoder.decode(data);

						EndianDataInputStream is = new EndianDataInputStream(new ByteArrayInputStream(data));

						is.order(ByteOrder.LITTLE_ENDIAN);

						int size = is.readInt();
						
						is.close();

						byte[] decoded = Arrays.copyOfRange(data, 4, data.length);

						decoded = Arrays.copyOfRange(decoded, 0, decoded.length);

						Inflater decompresser = new Inflater();
						decompresser.setInput(decoded, 0, decoded.length);

						byte[] result = new byte[size];
						int resultLength = decompresser.inflate(result);
						decompresser.end();

						Util.printBytes(result, 100, this);

//						String name = file + ".dec";
//						OutputStream os = new FileOutputStream(name);
//						os.write(result);
//						os.close();

						World world = new World(Edition.Mobile, result);

					} else if (file.endsWith(".world")) {

						World world = new World(Edition.Mobile, data);
						// TODO:
					} else if (file.endsWith(".w")) {
						log("Loading 3DS worlds not yet supported.");

					} else if (file.endsWith(".player")) {
						bl = new Blowfish(key);
						bl.reinit();
						log(Util.bytesToHex(data));
						bl.decipher(data, 2, data, 2, data.length - 2);

						log(Util.bytesToHex(data));

						Player p = new Player(Edition.Mobile);
						p.ParseMobileData(data);
					} else if (file.endsWith(".p")) {
						log("Loading 3DS players not yet supported.");
						// TODO:
					}

					if (writeFile) {
						// TODO: Save/Convert data..
						String decryptedName = file + ".dec";
						OutputStream os = new FileOutputStream(decryptedName);
						os.write(data);
						os.close();

						log("Created " + decryptedName);
					} else {
						log("File creation was disabled.");
					}

				} catch (Exception e) {
					log("ERROR: " + e.getMessage());
					e.printStackTrace();
				}
			} else {
				log("File not found. " + file);
			}
		} else {
			log("No file input.");
		}
	}

	private String bits(byte b) {
		return (Util.getBit(b, 7)//
				+ "" + Util.getBit(b, 6)//
				+ "" + Util.getBit(b, 5)//
				+ "" + Util.getBit(b, 4)//
				+ "" + Util.getBit(b, 3)//
				+ "" + Util.getBit(b, 2)//
				+ "" + Util.getBit(b, 1)//
				+ "" + Util.getBit(b, 0));
	}

	private void loadItems() {
		InputStream in = Main.class.getResourceAsStream("/data/ItemsMobile.txt");
		BufferedReader r = new BufferedReader(new InputStreamReader(in));
		String line = "";
		try {
			while ((line = r.readLine()) != null) {
				String[] split = line.split("=");
				// log("Adding " + split[1] + " as " + split[0]);
				ItemTypes.addItem(split[1], Integer.parseInt(split[0]), Edition.Mobile);
			}
		} catch (IOException e) {
			log(e.getMessage());
			e.printStackTrace();
		}
	}
}
