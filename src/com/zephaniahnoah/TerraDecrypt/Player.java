package com.zephaniahnoah.TerraDecrypt;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteOrder;

import github.MichaelBeeu.util.EndianDataInputStream;

enum Difficulty {
	Softcore("Classic"), //
	Mediumcore("Mediumcore"), //
	Hardcore("Hardcore"), //
	Journey("Journey");

	public String altName;

	Difficulty(String altName) {
		this.altName = altName;
	}
}

public class Player implements Log {
	public String name;
	public Difficulty dif;
	public Edition loadedFrom;
	public int hp, mana, maxHp, maxMana, hairDye;
	public Item[] items, armor, vanityArmor, accessories, vanityAccessories, dyes, bank, safe;
	public boolean gender;
	public byte hairStyle;
	public Color hair, skin, eyes, shirt, undershirt, pants, shoes;
	public Buff[] buffs;
	public boolean[] hiddenEquipment;
	private EndianDataInputStream is;

	public Player(Edition loadedFrom) {
		this.loadedFrom = loadedFrom;
		/*
		 * this.name = "Player"; this.dif = Difficulty.Softcore; this.hp = 500; this.mana = 200; this.maxHp = 500; this.maxMana = 200; this.items = new Item[47]; this.gender = true; this.hairStyle = 0; this.hairDye = 0; this.hair = new Color(0, 0, 0); this.skin = new Color(0, 0, 0); this.eyes = new Color(0, 0, 0); this.shirt = new Color(0, 0, 0); this.undershirt = new Color(0, 0, 0); this.pants = new Color(0, 0, 0); this.shoes = new Color(0, 0, 0); this.buffs = new Buff[20];
		 */
	}

	// Supported versions: 24 (Mobile), 22 (3DS)
	public void ParseMobileData(byte[] data) {
		log("Parsing mobile player file.");
		short version = -1;
		try {
			is = new EndianDataInputStream(new ByteArrayInputStream(data));
			is.order(ByteOrder.LITTLE_ENDIAN);
			version = is.readShort();
			log("Mobile player file version: " + version);
			int nameLength = is.readInt();

			byte[] nameBytes = new byte[nameLength];

			for (int i = 0; i < nameLength; i++) {
				nameBytes[i] = is.readByte();
				is.readByte();
			}

			name = new String(nameBytes, "UTF-8");
			log("Player name is " + name);

			if (version > 23) {
				dif = Difficulty.values()[is.readByte()];
				log("Difficulty is " + dif);
			}

			// Hair style
			hairStyle = is.readByte();
			log("Player hair style is #" + hairStyle);

			// Hair dye
			if (version > 23) {
				hairDye = is.readByte();
				log(hairDye == 0 ? "No hair dye." : "Hair dye is: " + hairDye);
			}

			gender = is.readByte() != 0;
			log("Gender is " + (gender ? "male" : "female"));

			hp = is.readShort();
			maxHp = is.readShort();
			mana = is.readShort();
			maxMana = is.readShort();
			log("HP: " + hp + "/" + maxHp);
			log("MANA: " + mana + "/" + maxMana);

			loadColor(hair);
			loadColor(skin);
			loadColor(eyes);
			loadColor(shirt);
			loadColor(undershirt);
			loadColor(pants);
			loadColor(shoes);

			armor = new Item[3];

			// Should each armor piece have it's own var?
			armor[0] = new Item(is.readShort(), 1, is.readByte());
			armor[1] = new Item(is.readShort(), 1, is.readByte());
			armor[2] = new Item(is.readShort(), 1, is.readByte());

			accessories = new Item[] { new Item(is.readShort(), 1, is.readByte()), //
					new Item(is.readShort(), 1, is.readByte()), new Item(is.readShort(), 1, is.readByte()), //
					new Item(is.readShort(), 1, is.readByte()), new Item(is.readShort(), 1, is.readByte()) };

			log("ARMOR - " + ItemTypes.getNameFromID(armor[0].id, Edition.Mobile) + " on head, "//
					+ ItemTypes.getNameFromID(armor[1].id, Edition.Mobile) + " on chest, and "//
					+ ItemTypes.getNameFromID(armor[2].id, Edition.Mobile) + " on legs...");

			log("EQUIPMENT --------------------");
			logItem(accessories[0]);
			logItem(accessories[1]);
			logItem(accessories[2]);
			logItem(accessories[3]);
			logItem(accessories[4]);

			vanityArmor = new Item[] { new Item(is.readShort(), 1, is.readByte()), new Item(is.readShort(), 1, is.readByte()), new Item(is.readShort(), 1, is.readByte()) };

			log("VANITY -----------------------");
			logItem(vanityArmor[0]);
			logItem(vanityArmor[1]);
			logItem(vanityArmor[2]);
			dyes = new Item[] { new Item(is.readShort()), new Item(is.readShort()), new Item(is.readShort()) };

			fillInventory(items = new Item[48]);

			// Unknown stuff
			// There is a 32byte number here stored as a hex value but as text... weird
			is.readInt();
			int lengthOfNumber = 32;
			byte[] unknownBytes = new byte[lengthOfNumber];
			for (int i = 0; i < lengthOfNumber; i++) {
				unknownBytes[i] = is.readByte();
			}

			String unknownString = new String(unknownBytes, "UTF-8");

			log("Unknow hex value: " + unknownString);

			log("BANK -------------------------");
			fillInventory(bank = new Item[40]);

			log("SAFE -------------------------");
			fillInventory(safe = new Item[40]);

			log("BUFFS ------------------------");
			buffs = new Buff[10];

			for (int i = 0; i < buffs.length; i++) {
				int id = is.readInt() & 0xFFFFFFFF;
				int time = is.readShort() & 0xFFFF;
				if (id != 0) {
					log("Found " + Data.buffs.get(id) + " for " + time + " ticks");
					buffs[i] = new Buff(id, time);
				}
			}

		} catch (Exception e) {
			log("Error while parsing player: " + e.getMessage());
			e.printStackTrace();
		}
	}

	// Item data order -> id(0x00 0x00) count(0x00 0x00) prefix(0x00) <- Five bytes
	// Should I really be doing 0xFFFF?? 0xFF may be better
	private void fillInventory(Item[] itemList) throws IOException {
		for (int i = 0; i < itemList.length; i++) {
			Item item = new Item(is.readShort() & 0xFFFF, is.readShort() & 0xFFFF, is.readByte());
			if (item.count != 0) {
				itemList[i] = item;
				logItem(item);
			}
		}
	}

	private void loadColor(Color c) throws IOException {
		c = new Color(is.readUnsignedByte() & 0xFF, is.readUnsignedByte() & 0xFF, is.readUnsignedByte() & 0xFF);
		log("Loaded color " + c.getRed() + " " + c.getGreen() + " " + c.getBlue());
	}

	private void logItem(Item i) {
		String prefix = Data.prefixes.get(i.prefix);
		String count = i.count + "";
		if (count.length() < 3) {
			if (count.length() == 1) {
				count = "00" + count;
			} else {
				count = "0" + count;
			}
		}
		log(count + " - " + (prefix == null ? "" : prefix + " ") + ItemTypes.getNameFromID(i.id, loadedFrom));
	}
}
