package com.zephaniahnoah.TerraDecrypt;

public class Tile implements Log {

	public enum BrickStyle {
		Full, //
		HalfBrick, //
		SlopeTopLeftDown, //
		SlopeBottomLeftDown, //
		SlopeTopLeftUp, //
		SlopeBottomLeftUp, //
		Unknown06, //
		Unknown07;
	}

	public enum LiquidType {
		None, //
		Water, //
		Lava, //
		Honey
	}

	private static boolean[] TileFrameImportantMobileOld = { false, false, false, true, true, true, false, false, false, false, true, true, true, true, true, true, true, true, true, false, true, true, false, false, true, false, true, true, true, true, false, true, false, true, true, true, true, false, false, false, false, false, true, false, false, false, false, false, false, false, true, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, true, true, true, false, false, true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, true, false, false, true, true, false, false, false, false, false, false, false, false, false, false, true, true, false, true, true, false, false, true, true, true, true, true, true, true, true, false, true, true, true, true, false, false, false, false, true, true };
	private static boolean[] TileFrameImportantMobileNew = { false, false, false, true, true, true, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, false, true, true, true, true, false, true, false, true, true, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, true, false, false, false, false, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, true, true, true, true, false, false, true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, true, false, false, true, true, false, false, false, false, false, false, false, false, false, false, true, true, false, true, true, false, false, true, true, true, true, true, true, true, true, false, true, true, true, true, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, false, true, true, false, false, false, true, false, false, false, false, false, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, true, false, true, true, false, true, false, false, true, true, true, true, true, true, false, false, false, false, false, false, true, true, false, false, true, false, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, false, false, false, true, true, true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, true, false, true, true, true, true, true, false, false, true, true, false, false, false, false, false, false, false, false, false, true, true, false, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true };

	public static final Tile Empty = new Tile();

	public byte mapVisibility; // 0-15
	public boolean isActive;
	public boolean wireRed;
	public boolean wireGreen;
	public boolean wireBlue;
	public byte tileColor;
	public char type;// Must be unsigned short. Best Java equiv is char
	public byte wall;
	public byte wallColor;
	public LiquidType liquidType;
	public byte liquidAmount;
	public BrickStyle brickStyle;
	public boolean actuator;
	public boolean inActive;
	public short U, V;

	public short uvTileCache = Short.MAX_VALUE; // Caches the UV position of a tile, since it is costly to generate each frame

	public short uvWallCache = Short.MAX_VALUE; // Caches the UV position of a wall tile

	public byte lazyMergeId = Byte.MAX_VALUE; // The ID here refers to a number that helps blocks know whether they are actually merged with a nearby tile

	public boolean hasLazyChecked = false; // Whether the above check has taken place

	public void Reset() {
		isActive = false;
		wireRed = false;
		wireGreen = false;
		wireBlue = false;
		tileColor = 0;
		type = 0;
		wall = 0;
		liquidType = LiquidType.None;
		wallColor = 0;
		liquidAmount = 0;
		brickStyle = BrickStyle.Full;
		actuator = false;
		inActive = false;
	}

	protected boolean tilesEqual(Tile other) {
		return isActive == other.isActive && //
				type == other.type && //
				tileColor == other.tileColor && //
				U == other.U && V == other.V && //
				liquidType == other.liquidType && //
				liquidAmount == other.liquidAmount && //
				wall == other.wall && //
				wallColor == other.wallColor && //
				wireRed == other.wireRed && //
				wireGreen == other.wireGreen && //
				wireBlue == other.wireBlue && //
				brickStyle == other.brickStyle && //
				brickStyle == other.brickStyle && //
				actuator == other.actuator && //
				inActive == other.inActive;
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj)
			return false;
		if (this == obj)
			return true;
		if (obj.getClass() != this.getClass())
			return false;
		return tilesEqual((Tile) obj);
	}

	// Referenced from TEdit. Idk if this works in Java.
	@Override
	public int hashCode() {
		int hashCode = Boolean.hashCode(isActive);
		hashCode = (hashCode * 397) ^ Boolean.hashCode(wireRed);
		hashCode = (hashCode * 397) ^ Boolean.hashCode(wireGreen);
		hashCode = (hashCode * 397) ^ Boolean.hashCode(wireBlue);
		hashCode = (hashCode * 397) ^ liquidType.hashCode();
		hashCode = (hashCode * 397) ^ Byte.hashCode(tileColor);
		hashCode = (hashCode * 397) ^ Byte.hashCode(wall);
		hashCode = (hashCode * 397) ^ Character.hashCode(type);
		hashCode = (hashCode * 397) ^ Byte.hashCode(wallColor);
		hashCode = (hashCode * 397) ^ Byte.hashCode(liquidAmount);
		hashCode = (hashCode * 397) ^ brickStyle.hashCode();
		hashCode = (hashCode * 397) ^ Boolean.hashCode(actuator);
		// hashCode = (hashCode * 397) ^ brickStyle.hashCode();
		hashCode = (hashCode * 397) ^ Boolean.hashCode(inActive);
		hashCode = (hashCode * 397) ^ Short.hashCode(U);
		hashCode = (hashCode * 397) ^ Short.hashCode(V);
		return hashCode;

	}

	static public char UpdateTypeMobile(char oldType) {
		switch (oldType) {
		case 35:
		case 36:
			return 34;
		case 150:
			return 500;
		default:
			return oldType;
		}
	}

	static public int FrameImportantMobile(int type, int version) {
		// 0 - not important (set to -1)
		// 1 - important (read)
		// 2 - became important (set to 0)

		if (version > 57) {
			return (type < TileFrameImportantMobileNew.length && TileFrameImportantMobileNew[type]) ? 1 : 0;
		}
		if (type < TileFrameImportantMobileOld.length && TileFrameImportantMobileOld[type]) {
			return 1;
		}
		return (type < TileFrameImportantMobileNew.length && TileFrameImportantMobileNew[type]) ? 2 : 0;
	}

	public void UpdateTileFrameFromMobile() {
		if (type == 314) {
			// Apparently V is the second sprite. Ignore it?
			switch (U) {
			case 0:
			case 1:
			case 2:
			case 3:
				V = (short) (U / 2);
				break;
			case 4:
			case 5:
				U -= 4;
				V = 2;
				break;
			case 6:
			case 7:
				U -= 6;
				V = 1;
				break;
			case 8:
			case 9:
				U -= 8;
				V = 3;
				break;
			case 10:
			case 11:
			case 12:
			case 13:
				U -= 6;
				V = 1;
				break;
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
				U -= 12;
				V = 0;
				break;
			case 20:
			case 21:
				U -= 20;
				V = 4;
				break;
			case 22:
			case 23:
				U -= 22;
				V = 5;
				break;
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
				U -= 22;
				V = 2;
				break;
			case 30:
			case 31:
			case 32:
			case 33:
			case 34:
			case 35:
				U -= 28;
				V = 3;
				break;
			case 36:
			case 37:
				U -= 36;
				V = 6;
				break;
			case 38:
				U = 1;
				V = 7;
				break;
			case 39:
				U = 0;
				V = 7;
				break;
			}
			if (U > -1)
				U *= 18;
			if (V > -1)
				V *= 18;
		}
	}

	public Tile copy() {
		Tile copy = new Tile();

		// copy.Reset();

		copy.mapVisibility = mapVisibility; // 0-15
		copy.isActive = isActive;
		copy.wireRed = wireRed;
		copy.wireGreen = wireGreen;
		copy.wireBlue = wireBlue;
		copy.tileColor = tileColor;
		copy.type = type;
		copy.wall = wall;
		copy.wallColor = wallColor;
		copy.liquidType = liquidType;
		copy.liquidAmount = liquidAmount;
		copy.brickStyle = brickStyle;
		copy.actuator = actuator;
		copy.inActive = inActive;
		copy.U = U;
		copy.V = V;

//		copy.uvTileCache = uvTileCache;
//		copy.uvWallCache = uvWallCache;
//		copy.lazyMergeId = lazyMergeId;
//		copy.hasLazyChecked = hasLazyChecked;

		if (!tilesEqual(copy)) {
			log("Failed to copy tile!");
		}

		return copy;
	}

//	public static boolean operator==(
//	Tile left, Tile right)
//	{
//		return Equals(left, right);
//	}
//
//	public static boolean operator!=(
//	Tile left, Tile right)
//	{
//		return !Equals(left, right);
//	}
}
