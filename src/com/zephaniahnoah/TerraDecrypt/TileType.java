package com.zephaniahnoah.TerraDecrypt;

import java.awt.Color;

public enum TileType {
	Dirt(102, 70, 30), //
	Stone(82, 82, 82), //
	Grass(63, 175, 34), //
	Plants(42, 90, 19), //
	Torches(255, 135, 0), //
	Trees(131, 88, 40), //
	Iron(144, 125, 57), //
	Copper(189, 117, 5), //
	Gold(245, 224, 41), //
	Silver(237, 237, 237), //
	ClosedDoor(124, 95, 55), //
	OpenDoor(124, 95, 55), //
	Heart(238, 19, 112), //
	Bottles(175, 213, 207), //
	Tables(), //
	Chairs(), //
	Anvils(), //
	Furnaces(), //
	WorkBenches(), //
	Platforms(), //
	Saplings(), //
	Containers(), //
	Demonite(84, 14, 102), //
	CorruptGrass(115, 34, 154), //
	CorruptPlants(162, 91, 171), //
	Ebonstone(75, 32, 79), //
	DemonAltar(149, 16, 97), //
	Sunflower(224, 251, 96), //
	Pots(123, 87, 62), //
	PiggyBank(254, 143, 134), //
	WoodBlock(131, 88, 40), //
	ShadowOrbs(173, 130, 201), //
	CorruptThorns(205, 156, 201), //
	Candles(252, 248, 185), //
	Chandeliers(180, 180, 180), // TODO:
	Jackolanterns(255, 160, 50), //
	Presents(251, 80, 60), //
	Meteorite(105, 46, 15), //
	GrayBrick(156, 156, 156), //
	RedBrick(157, 78, 78), //
	ClayBlock(126, 48, 41), //
	BlueDungeonBrick(56, 53, 97), //
	HangingLanterns(0, 255, 0), // TODO:
	GreenDungeonBrick(0, 67, 38), //
	PinkDungeonBrick(113, 31, 36), //
	GoldBrick(190, 161, 0), //
	SilverBrick(224, 224, 224), //
	CopperBrick(217, 137, 0), //
	Spikes(117, 117, 117), //
	WaterCandle(0, 116, 231), //
	Books(84, 0, 85), // TODO:
	Cobweb(238, 238, 238), //
	Vines(55, 158, 44), //
	Sand(231, 211, 73), //
	Glass(240, 240, 240), //
	Signs(102, 49, 17), //
	Obsidian(53, 0, 77), //
	Ash(64, 56, 46), //
	Hellstone(99, 48, 38), //
	Mud(75, 49, 19), //
	JungleGrass(122, 221, 14), //
	JunglePlants(90, 157, 18), //
	JungleVines(120, 206, 28), //
	Sapphire(0, 2, 245), //
	Ruby(255, 0, 15), //
	Emerald(35, 255, 51), //
	Topaz(255, 201, 0), //
	Amethyst(224, 3, 255), //
	Diamond(229, 251, 255), //
	JungleThorns(200, 100, 50), //
	MushroomGrass(58, 80, 216), //
	MushroomPlants(92, 111, 228), //
	MushroomTrees(50, 50, 91), //
	Plants2(), //
	JunglePlants2(), //
	ObsidianBrick(), //
	HellstoneBrick(), //
	Hellforge(), //
	ClayPot(), //
	Beds(), //
	Cactus(), //
	Coral(), //
	ImmatureHerbs(), //
	MatureHerbs(), //
	BloomingHerbs(), //
	Tombstones(), //
	Loom(), //
	Pianos(), //
	Dressers(), //
	Benches(), //
	Bathtubs(), //
	Banners(), //
	Lampposts(), //
	Lamps(), //
	Kegs(), //
	ChineseLanterns(), //
	CookingPots(), //
	Safes(), //
	SkullLanterns(), //
	TrashCan(), //
	Candelabras(), //
	Bookcases(), //
	Thrones(), //
	Bowls(), //
	GrandfatherClocks(), //
	Statues(), //
	Sawmill(), //
	Cobalt(), //
	Mythril(), //
	HallowedGrass(), //
	HallowedPlants(), //
	Adamantite(), //
	Ebonsand(), //
	HallowedPlants2(), //
	TinkerersWorkbench(), //
	HallowedVines(), //
	Pearlsand(255, 251, 223), //
	Pearlstone(196, 150, 169), //
	PearlstoneBrick(), //
	IridescentBrick(), //
	Mudstone(), //
	CobaltBrick(), //
	MythrilBrick(), //
	Silt(), //
	WoodenBeam(), //
	CrystalBall(), //
	DiscoBall(), //
	MagicalIceBlock(), //
	Mannequin(), //
	Crystals(), //
	ActiveStoneBlock(), //
	InactiveStoneBlock(), //
	Lever(), //
	AdamantiteForge(), //
	MythrilAnvil(), //
	PressurePlates(), //
	Switches(), //
	Traps(), //
	Boulder(), //
	MusicBoxes(), //
	DemoniteBrick(), //
	Explosives(), //
	InletPump(), //
	OutletPump(), //
	Timers(), //
	CandyCaneBlock(), //
	GreenCandyCaneBlock(), //
	SnowBlock(240, 240, 240), //
	SnowBrick(), //
	HolidayLights(), //
	AdamantiteBeam(), //
	SandstoneBrick(), //
	EbonstoneBrick(), //
	RedStucco(), //
	YellowStucco(), //
	GreenStucco(), //
	GrayStucco(), //
	Ebonwood(), //
	RichMahogany(), //
	Pearlwood(), //
	RainbowBrick(), //
	IceBlock(146, 228, 255), //
	BreakableIce(), //
	CorruptIce(), //
	HallowedIce(183, 168, 194), //
	Stalactite(), //
	Tin(), //
	Lead(), //
	Tungsten(), //
	Platinum(), //
	PineTree(), //
	ChristmasTree(), //
	Sinks(), //
	PlatinumCandelabra(), //
	PlatinumCandle(), //
	TinBrick(), //
	TungstenBrick(), //
	PlatinumBrick(), //
	ExposedGems(), //
	GreenMoss(), //
	BrownMoss(), //
	RedMoss(), //
	BlueMoss(), //
	PurpleMoss(), //
	LongMoss(), //
	SmallPiles(), //
	LargePiles(), //
	LargePiles2(), //
	CactusBlock(), //
	Cloud(235, 240, 235), //
	MushroomBlock(), //
	LivingWood(131, 88, 40), //
	LeafBlock(18, 75, 14), // TODO:
	SlimeBlock(), //
	BoneBlock(), //
	FleshBlock(), //
	RainCloud(), //
	FrozenSlimeBlock(), //
	Asphalt(), //
	FleshGrass(), //
	FleshIce(), //
	FleshWeeds(), //
	Sunplate(), //
	Crimstone(102, 21, 26), //
	Crimtane(), //
	CrimsonVines(), //
	IceBrick(), //
	WaterFountain(), //
	Shadewood(), //
	Cannon(), //
	LandMine(), //
	Chlorophyte(), //
	SnowballLauncher(), //
	Rope(), //
	Chain(), //
	Campfire(), //
	Firework(), //
	Blendomatic(), //
	MeatGrinder(), //
	Extractinator(), //
	Solidifier(), //
	Palladium(), //
	Orichalcum(), //
	Titanium(), //
	Slush(), //
	Hive(216, 187, 57), //
	LihzahrdBrick(161, 83, 12), // TODO:
	DyePlants(), //
	DyeVat(), //
	HoneyBlock(), //
	CrispyHoneyBlock(), //
	Larva(), //
	WoodenSpikes(), //
	PlantDetritus(), //
	Crimsand(), //
	Teleporter(), //
	LifeFruit(), //
	LihzahrdAltar(), //
	PlanteraBulb(), //
	MetalBars(), //
	Painting3X3(), //
	Painting4X3(), //
	Painting6X4(), //
	ImbuingStation(), //
	BubbleMachine(), //
	Painting2X3(), //
	Painting3X2(), //
	Autohammer(), //
	PalladiumColumn(), //
	BubblegumBlock(), //
	Titanstone(), //
	PumpkinBlock(), //
	HayBlock(), //
	SpookyWood(), //
	Pumpkins(), //
	AmethystGemsparkOff(), //
	TopazGemsparkOff(), //
	SapphireGemsparkOff(), //
	EmeraldGemsparkOff(), //
	RubyGemsparkOff(), //
	DiamondGemsparkOff(), //
	AmberGemsparkOff(), //
	AmethystGemspark(), //
	TopazGemspark(), //
	SapphireGemspark(), //
	EmeraldGemspark(), //
	RubyGemspark(), //
	DiamondGemspark(), //
	AmberGemspark(), //
	Womannequin(), //
	FireflyinaBottle(), //
	LightningBuginaBottle(), //
	Cog(), //
	StoneSlab(), //
	SandStoneSlab(), //
	BunnyCage(), //
	SquirrelCage(), //
	MallardDuckCage(), //
	DuckCage(), //
	BirdCage(), //
	BlueJay(), //
	CardinalCage(), //
	FishBowl(), //
	HeavyWorkBench(), //
	CopperPlating(), //
	SnailCage(), //
	GlowingSnailCage(), //
	AmmoBox(), //
	MonarchButterflyJar(), //
	PurpleEmperorButterflyJar(), //
	RedAdmiralButterflyJar(), //
	UlyssesButterflyJar(), //
	SulphurButterflyJar(), //
	TreeNymphButterflyJar(), //
	ZebraSwallowtailButterflyJar(), //
	JuliaButterflyJar(), //
	ScorpionCage(), //
	BlackScorpionCage(), //
	FrogCage(), //
	MouseCage(), //
	BoneWelder(), //
	FleshCloningVat(), //
	GlassKiln(), //
	LihzahrdFurnace(), //
	LivingLoom(), //
	SkyMill(), //
	IceMachine(), //
	SteampunkBoiler(), //
	HoneyDispenser(), //
	PenguinCage(), //
	WormCage(), //
	DynastyWood(), //
	RedDynastyShingles(), //
	BlueDynastyShingles(), //
	MinecartTrack(), //
	Coralstone(), //
	BlueJellyfishBowl(), //
	GreenJellyfishBowl(), //
	PinkJellyfishBowl(), //
	ShipInABottle(), //
	SeaweedPlanter(), //
	BorealWood(), //
	PalmWood(), //
	PalmTree(), //
	BeachPiles(), //
	TinPlating(), //
	Waterfall(), //
	Lavafall(), //
	Confetti(), //
	ConfettiBlack(), //
	CopperCoinPile(), //
	SilverCoinPile(), //
	GoldCoinPile(), //
	PlatinumCoinPile(), //
	WeaponsRack(), //
	FireworksBox(), //
	LivingFire(), //
	AlphabetStatues(), //
	FireworkFountain(), //
	GrasshopperCage(), //
	LivingCursedFire(), //
	LivingDemonFire(), //
	LivingFrostFire(), //
	LivingIchor(), //
	LivingUltrabrightFire(), //
	Honeyfall(), //
	ChlorophyteBrick(), //
	CrimtaneBrick(), //
	ShroomitePlating(), //
	MushroomStatue(), //
	MartianConduitPlating(), //
	ChimneySmoke(), //
	CrimtaneThorns(), //
	VineRope(), //
	BewitchingTable(), //
	AlchemyTable(), //
	Sundial(), //
	MarbleBlock(), //
	GoldBirdCage(), //
	GoldBunnyCage(), //
	GoldButterflyCage(), //
	GoldFrogCage(), //
	GoldGrasshopperCage(), //
	GoldMouseCage(), //
	GoldWormCage(), //
	SilkRope(), //
	WebRope(), //
	Marble(), //
	Granite(), //
	GraniteBlock(), //
	MeteoriteBrick(), //
	PinkSlimeBlock(), //
	PeaceCandle(), //
	WaterDrip(), //
	LavaDrip(), //
	HoneyDrip(), //
	FishingCrate(), //
	SharpeningStation(), //
	TargetDummy(), //
	Bubble(), //
	PlanterBox(), //
	LavaMoss(), //
	VineFlowers(), //
	LivingMahogany(), //
	LivingMahoganyLeaves(), //
	CrystalBlock(), //
	TrapdoorOpen(), //
	TrapdoorClosed(), //
	TallGateClosed(), //
	TallGateOpen(), //
	LavaLamp(), //
	CageEnchantedNightcrawler(), //
	CageBuggy(), //
	CageGrubby(), //
	CageSluggy(), //
	ItemFrame(), //
	Sandstone(), //
	HardenedSand(), //
	CorruptHardenedSand(), //
	CrimsonHardenedSand(), //
	CorruptSandstone(), //
	CrimsonSandstone(), //
	HallowHardenedSand(), //
	HallowSandstone(), //
	DesertFossil(), //
	Fireplace(), //
	Chimney(), //
	FossilOre(), //
	LunarOre(), //
	LunarBrick(), //
	LunarMonolith(), //
	Detonator(), //
	LunarCraftingStation(), //
	SquirrelOrangeCage(), //
	SquirrelGoldCage(), //
	LunarBlockSolar(), //
	LunarBlockVortex(), //
	LunarBlockNebula(), //
	LunarBlockStardust(), //
	LogicGateLamp(), //
	LogicGate(), //
	ConveyorBeltLeft(), //
	ConveyorBeltRight(), //
	LogicSensor(), //
	WirePipe(), //
	AnnouncementBox(), //
	TeamBlockRed(), //
	TeamBlockRedPlatform(), //
	WeightedPressurePlate(), //
	WireBulb(), //
	TeamBlockGreen(), //
	TeamBlockBlue(), //
	TeamBlockYellow(), //
	TeamBlockPink(), //
	TeamBlockWhite(), //
	TeamBlockGreenPlatform(), //
	TeamBlockBluePlatform(), //
	TeamBlockYellowPlatform(), //
	TeamBlockPinkPlatform(), //
	TeamBlockWhitePlatform(), //
	GemLocks(), //
	FakeContainers(), //
	ProjectilePressurePad(), //
	GeyserTrap(), //
	BeeHive(), //
	PixelBox(), //
	SillyBalloonPink(), //
	SillyBalloonPurple(), //
	SillyBalloonGreen(), //
	SillyStreamerBlue(), //
	SillyStreamerGreen(), //
	SillyStreamerPink(), //
	SillyBalloonMachine(), //
	SillyBalloonTile(), //
	Pigronata(), //
	PartyMonolith(), //
	PartyBundleOfBalloonTile(), //
	PartyPresent(), //
	SandFallBlock(), //
	SnowFallBlock(), //
	SnowCloud(), //
	SandDrip(), //
	DjinnLamp(), //
	DefendersForge(), //
	WarTable(), //
	WarTableBanner(), //
	ElderCrystalStand(), //
	Containers2(), //
	FakeContainers2(), //
	Tables2();

	public Color c;

	TileType(int r, int g, int b) {
		c = new Color(r, g, b);
	}

	TileType() {
		c = Color.GRAY;
	}

	public static Color getColorFromID(int id) {
		if (id > values().length - 1) {
			return Color.black;
		}
		return TileType.values()[id].c;
	}
}