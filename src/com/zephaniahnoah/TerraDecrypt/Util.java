package com.zephaniahnoah.TerraDecrypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class Util {

	private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static short bytesToShort(byte a, byte b) {
		short sh = (short) a;
		sh <<= 8;
		short ret = (short) (sh | b);
		return ret;
	}

	public static byte[] getBytesFromFile(File file) throws IOException {
		long length = file.length();

		if (length > Integer.MAX_VALUE) {
			throw new IOException("File is too large!");
		}

		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;

		InputStream is = new FileInputStream(file);
		try {
			while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
		} finally {
			is.close();
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}
		return bytes;
	}

	public static int getBit(byte ID, int position) {
		return (ID >> position) & 1;
	}

	public static void printBytes(byte[] data, int length, Log log) {
		log.log(bytesToHex(Arrays.copyOf(data, length)));
	}
}
