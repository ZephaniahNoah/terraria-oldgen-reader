package com.zephaniahnoah.TerraDecrypt;

import java.awt.Color;

public enum Wall {

	Air(0, 0, 0), //
	Stone(50, 50, 50), //
	DirtUnsafe(76, 54, 32), //
	Ebonstone(TileType.Ebonstone), //
	Unknown4(127, 127, 127), // TODO
	Unknown5(127, 127, 127), // TODO
	Unknown6(127, 127, 127), // TODO
	BlueDung7(TileType.BlueDungeonBrick), //
	Dungeon8(127, 127, 127), // TODO
	GreenDungeonUnsafe(TileType.GreenDungeonBrick), //
	PinkDungeonUnsafe(TileType.PinkDungeonBrick), //
	Unknown11(127, 127, 127), // TODO
	Unknown12(127, 127, 127), // TODO
	HellStoneOrObsidian13(255, 255, 0), // TODO
	ObsidianOrHellstone14(255, 255, 0), // TODO
	LowerJungle15(127, 127, 127), // TODO
	Unknown16(127, 127, 127), // TODO
	Unknown17(127, 127, 127), // TODO
	Unknown18(127, 127, 127), // TODO
	Unknown19(127, 127, 127), // TODO
	Unknown20(127, 127, 127), // TODO
	Unknown21(127, 127, 127), // TODO
	Unknown22(127, 127, 127), // TODO
	Unknown23(127, 127, 127), // TODO
	Unknown24(127, 127, 127), // TODO
	Unknown25(127, 127, 127), // TODO
	Unknown26(127, 127, 127), // TODO
	Planked(70, 49, 30), //
	Unknown28(127, 127, 127), // TODO
	Unknown29(127, 127, 127), // TODO
	Unknown30(127, 127, 127), // TODO
	Unknown31(127, 127, 127), // TODO
	Unknown32(127, 127, 127), // TODO
	Unknown33(127, 127, 127), // TODO
	Unknown34(127, 127, 127), // TODO
	Unknown35(127, 127, 127), // TODO
	Unknown36(127, 127, 127), // TODO
	Unknown37(127, 127, 127), // TODO
	Unknown38(127, 127, 127), // TODO
	Unknown39(127, 127, 127), // TODO
	Snow(TileType.SnowBlock), //
	Unknown41(127, 127, 127), // TODO
	Unknown42(127, 127, 127), // TODO
	Unknown43(127, 127, 127), // TODO
	Unknown44(127, 127, 127), // TODO
	Unknown45(127, 127, 127), // TODO
	Unknown46(127, 127, 127), // TODO
	Unknown47(127, 127, 127), // TODO
	Unknown48(127, 127, 127), // TODO
	Unknown49(127, 127, 127), // TODO
	Unknown50(127, 127, 127), // TODO
	Unknown51(127, 127, 127), // TODO
	Unknown52(127, 127, 127), // TODO
	Unknown53(127, 127, 127), // TODO
	Unknown54(127, 127, 127), // TODO
	Cave55(127, 127, 127), // TODO
	Unknown56(127, 127, 127), // TODO
	Cave57(127, 127, 127), // TODO
	Unknown58(127, 127, 127), // TODO
	Cave59(127, 127, 127), // TODO
	Unknown60(127, 127, 127), // TODO
	Cave61(127, 127, 127), // TODO
	SpiderCave(51, 45, 41), //
	GrassWall(TileType.Grass), //
	JungleWall(48, 35, 5), //
	Unknown65(127, 127, 127), // TODO
	Unknown66(127, 127, 127), // TODO
	Unknown67(127, 127, 127), // TODO
	Unknown68(127, 127, 127), // TODO
	Unknown69(127, 127, 127), // TODO
	Unknown70(127, 127, 127), // TODO
	Unknown71(127, 127, 127), // TODO
	Unknown72(127, 127, 127), // TODO
	Unknown73(127, 127, 127), // TODO
	Unknown74(127, 127, 127), // TODO
	Unknown75(127, 127, 127), // TODO
	Unknown76(127, 127, 127), // TODO
	Unknown77(127, 127, 127), // TODO
	Unknown78(127, 127, 127), // TODO
	Cave79(127, 127, 127), // TODO
	MushroomWall(41, 54, 71), //
	Unknown81(127, 127, 127), // TODO
	Unknown82(127, 127, 127), // TODO
	Crimstone(TileType.Crimstone), //
	Unknown84(127, 127, 127), // TODO
	Unknown85(127, 127, 127), // TODO
	Hive(TileType.Hive), //
	LihzahrdBrick(TileType.LihzahrdBrick), //
	Unknown88(127, 127, 127), // TODO
	Unknown89(127, 127, 127), // TODO
	Unknown90(127, 127, 127), // TODO
	Unknown91(127, 127, 127), // TODO
	Unknown92(127, 127, 127), // TODO
	Unknown93(127, 127, 127), // TODO
	Unknown94(127, 127, 127), // TODO
	Unknown95(127, 127, 127), // TODO
	Unknown96(127, 127, 127), // TODO
	Unknown97(127, 127, 127), // TODO
	DungeonBrick98(TileType.GreenDungeonBrick), // TODO
	DungeonBrick99(TileType.GreenDungeonBrick), // TODO
	Unknown100(127, 127, 127), // TODO
	Unknown101(127, 127, 127), // TODO
	Unknown102(127, 127, 127), // TODO
	Unknown103(127, 127, 127), // TODO
	Unknown104(127, 127, 127), // TODO
	Unknown105(127, 127, 127), // TODO
	Unknown106(127, 127, 127), // TODO
	Unknown107(127, 127, 127), // TODO
	Unknown108(127, 127, 127), // TODO
	Unknown109(127, 127, 127), // TODO
	Unknown110(127, 127, 127), // TODO
	Unknown111(127, 127, 127), // TODO
	Unknown112(127, 127, 127), // TODO
	Unknown113(127, 127, 127), // TODO
	Unknown114(127, 127, 127), // TODO
	Unknown115(127, 127, 127), // TODO
	Unknown116(127, 127, 127), // TODO
	Unknown117(127, 127, 127), // TODO
	Unknown118(127, 127, 127), // TODO
	Unknown119(127, 127, 127), // TODO
	Unknown120(127, 127, 127), // TODO
	Unknown121(127, 127, 127), // TODO
	Unknown122(127, 127, 127), // TODO
	Unknown123(127, 127, 127), // TODO
	Unknown124(127, 127, 127), // TODO
	Unknown125(127, 127, 127), // TODO
	Unknown126(127, 127, 127), // TODO
	Unknown127(127, 127, 127), // TODO
	Unknown128(127, 127, 127), // TODO
	Unknown129(127, 127, 127), // TODO
	Unknown130(127, 127, 127), // TODO
	Unknown131(127, 127, 127), // TODO
	Unknown132(127, 127, 127), // TODO
	Unknown133(127, 127, 127), // TODO
	Unknown134(127, 127, 127), // TODO
	Unknown135(127, 127, 127), // TODO
	Unknown136(127, 127, 127), // TODO
	Unknown137(127, 127, 127), // TODO
	Unknown138(127, 127, 127), // TODO
	Unknown139(127, 127, 127), // TODO
	Unknown140(127, 127, 127), // TODO
	Unknown141(127, 127, 127), // TODO
	Unknown142(127, 127, 127), // TODO
	Unknown143(127, 127, 127), // TODO
	Unknown144(127, 127, 127), // TODO
	Unknown145(127, 127, 127), // TODO
	Unknown146(127, 127, 127), // TODO
	Unknown147(127, 127, 127), // TODO
	Unknown148(127, 127, 127), // TODO
	Unknown149(127, 127, 127), // TODO
	Unknown150(127, 127, 127), // TODO
	Unknown151(127, 127, 127), // TODO
	Unknown152(127, 127, 127), // TODO
	Unknown153(127, 127, 127), // TODO
	Unknown154(127, 127, 127), // TODO
	Unknown155(127, 127, 127), // TODO
	Unknown156(127, 127, 127), // TODO
	Unknown157(127, 127, 127), // TODO
	Unknown158(127, 127, 127), // TODO
	Unknown159(127, 127, 127), // TODO
	Unknown160(127, 127, 127), // TODO
	Unknown161(127, 127, 127), // TODO
	Unknown162(127, 127, 127), // TODO
	Unknown163(127, 127, 127), // TODO
	Unknown164(127, 127, 127), // TODO
	Unknown165(127, 127, 127), // TODO
	Unknown166(127, 127, 127), // TODO
	Unknown167(127, 127, 127), // TODO
	Unknown168(127, 127, 127), // TODO
	Unknown169(127, 127, 127), // TODO
	Unknown170(127, 127, 127), // TODO
	Unknown171(127, 127, 127), // TODO
	Unknown172(127, 127, 127), // TODO
	Unknown173(127, 127, 127), // TODO
	Unknown174(127, 127, 127), // TODO
	Unknown175(127, 127, 127), // TODO
	Unknown176(127, 127, 127), // TODO
	Unknown177(127, 127, 127), // TODO
	Unknown178(127, 127, 127), // TODO
	Unknown179(127, 127, 127), // TODO
	Unknown180(127, 127, 127), // TODO
	Unknown181(127, 127, 127), // TODO
	Unknown182(127, 127, 127), // TODO
	Unknown183(127, 127, 127), // TODO
	Unknown184(127, 127, 127), // TODO
	Unknown185(127, 127, 127), // TODO
	Unknown186(127, 127, 127), // TODO
	Unknown187(127, 127, 127), // TODO
	Unknown188(127, 127, 127), // TODO
	Unknown189(127, 127, 127), // TODO
	Unknown190(127, 127, 127), // TODO
	Unknown191(127, 127, 127), // TODO
	Unknown192(127, 127, 127), // TODO
	Unknown193(127, 127, 127), // TODO
	Unknown194(127, 127, 127), // TODO
	Unknown195(127, 127, 127), // TODO
	Unknown196(127, 127, 127), // TODO
	Unknown197(127, 127, 127), // TODO
	Unknown198(127, 127, 127), // TODO
	Unknown199(127, 127, 127), // TODO
	Unknown200(127, 127, 127), // TODO
	Unknown201(127, 127, 127), // TODO
	Unknown202(127, 127, 127), // TODO
	Unknown203(127, 127, 127), // TODO
	Unknown204(127, 127, 127), // TODO
	Unknown205(127, 127, 127), // TODO
	Unknown206(127, 127, 127), // TODO
	Unknown207(127, 127, 127), // TODO
	Unknown208(127, 127, 127), // TODO
	Unknown209(127, 127, 127), // TODO
	Unknown210(127, 127, 127), // TODO
	Unknown211(127, 127, 127), // TODO
	Unknown212(127, 127, 127), // TODO
	Unknown213(127, 127, 127), // TODO
	Unknown214(127, 127, 127), // TODO
	Unknown215(127, 127, 127), // TODO
	Unknown216(127, 127, 127), // TODO
	Unknown217(127, 127, 127), // TODO
	Unknown218(127, 127, 127), // TODO
	Unknown219(127, 127, 127), // TODO
	Unknown220(127, 127, 127), // TODO
	Unknown221(127, 127, 127), // TODO
	Unknown222(127, 127, 127), // TODO
	Unknown223(127, 127, 127), // TODO
	Unknown224(127, 127, 127), // TODO
	Unknown225(127, 127, 127), // TODO
	Unknown226(127, 127, 127), // TODO
	Unknown227(127, 127, 127), // TODO
	Unknown228(127, 127, 127), // TODO
	Unknown229(127, 127, 127), // TODO
	Unknown230(127, 127, 127), // TODO
	Unknown231(127, 127, 127), // TODO
	Unknown232(127, 127, 127), // TODO
	Unknown233(127, 127, 127), // TODO
	Unknown234(127, 127, 127), // TODO
	Unknown235(127, 127, 127), // TODO
	Unknown236(127, 127, 127), // TODO
	Unknown237(127, 127, 127), // TODO
	Unknown238(127, 127, 127), // TODO
	Unknown239(127, 127, 127), // TODO
	Unknown240(127, 127, 127), // TODO
	Unknown241(127, 127, 127), // TODO
	Unknown242(127, 127, 127), // TODO
	Unknown243(127, 127, 127), // TODO
	Unknown244(127, 127, 127), // TODO
	Unknown245(127, 127, 127), // TODO
	Unknown246(127, 127, 127), // TODO
	Unknown247(127, 127, 127), // TODO
	Unknown248(127, 127, 127), // TODO
	Unknown249(127, 127, 127), // TODO
	Unknown250(127, 127, 127), // TODO
	Unknown251(127, 127, 127), // TODO
	Unknown252(127, 127, 127), // TODO
	Unknown253(127, 127, 127), // TODO
	Unknown254(127, 127, 127), // TODO
	Unknown255(127, 127, 127);// TODO

	public Color color;

	Wall(int r, int g, int b) {
		color = new Color(r, g, b);
	}

	Wall(TileType t) {
		color = t.c.darker();
	}
}