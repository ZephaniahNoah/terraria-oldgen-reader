package com.zephaniahnoah.TerraDecrypt;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.zephaniahnoah.TerraDecrypt.Tile.BrickStyle;
import com.zephaniahnoah.TerraDecrypt.Tile.LiquidType;

import MarcusD.TerraInvedit.encryption.Blowfish;
import github.MichaelBeeu.util.EndianDataInputStream;

enum WorldSize {
	Edpanded3DS(4200, 1100, "Expanded"), //
	NormalOldGen(1750, 1000, "Normal"), //
	Tiny(1750, 900, "Normal"), //
	Small(4200, 1200, "Expanded"), //
	Medium(6400, 1800, "No Name"), //
	Large(8400, 2400, Medium.mobile), //
	UNKNOWN(0, 0, "UNKNOWN");

	public int width;
	public int height;
	public String mobile;

	private WorldSize(int width, int height, String mobile) {
		this.width = width;
		this.height = height;
		this.mobile = mobile;
	}
}

public class World implements Log {

	public static byte maxMoons = 3;
	public List<Integer> killCount = new ArrayList<Integer>();
	public List<String> playersWhoCompletedQuestToday = new ArrayList<String>();

	public Edition edition;
	public WorldSize size;
	public short width;
	public short height;
	public Tile[][] tiles;
	public boolean isCloudSave;
	public int worldId;
	public int creationTimestamp;
	public int rightWorld;
	public short bottomWorld;
	public short spawnX;
	public short spawnY;
	public short groundLevel;
	public short rockLevel;
	public short hellLevel;
	public float time;
	public boolean isDay;
	public byte moonPhase;
	public boolean bloodMoon;
	public short days;
	public boolean isEclipse;
	public byte moonType;
	public short dungeonX;
	public short dungeonY;
	public boolean isCrimson;
	public boolean tempRaining;
	public int tempRainTime;
	public float tempMaxRain;
	public float cloudBgActive;
	private Random rand;
	public boolean eyeOfCthulhuDowned;
	public boolean eaterOfWorldsDowned;
	public boolean skeletronDowned;
	public boolean savedGoblin;
	public boolean savedWizard;
	public boolean savedMechanic;
	public boolean savedStylist;
	public boolean savedAngler;
	public boolean downedGoblins;
	public boolean downedClown;
	public boolean downedFrostLegion;
	public boolean downedRabbit;
	public boolean downedTurkor;
	public boolean downedQueenBee;
	public boolean downedDestroyer;
	public boolean downedTwins;
	public boolean downedSkeletronPrime;
	public boolean downedMechBossAny;
	public boolean downedPlantBoss;
	public boolean downedGolemBoss;
	public boolean downedPirates;
	public boolean shadowOrbSmashed;
	public boolean spawnMeteor;
	public byte shadowOrbCount;
	public int altarCount;
	public short oreTier1;
	public short oreTier2;
	public short oreTier3;
	public boolean hardMode;
	public short hardModeDayCount;
	public byte invasionDelay;
	public short invasionSize;
	public byte invasionType;
	public float invasionX;
	public short anglerQuest;
	public boolean questCompletedToday;
	public byte treeStyle0;
	public byte treeStyle1;
	public byte treeStyle2;
	public byte treeStyle3;
	public short treeX0;
	public short treeX1;
	public short treeX2;
	public byte caveBackStyle0;
	public byte caveBackStyle1;
	public byte caveBackStyle2;
	public byte caveBackStyle3;
	public short caveBackX0;
	public short caveBackX1;
	public short caveBackX2;
	public int iceBackStyle;
	public int hellBackStyle;
	public int jungleBackStyle;
	public byte bgTree;
	public byte bgCorruption;
	public byte bgJungle;
	public byte bgSnow;
	public int[] caveBackX = new int[3];// TODO: Correct size?
	public byte bgHallow;
	public byte bgCrimson;
	public byte bgDesert;
	public byte bgOcean;
	public short copper;
	public short iron;
	public short silver;
	public short gold;

	// normal world size = 1750 × 900
	// expanded world size = 4200 × 1200

	public World(Edition edition, byte[] data) throws IOException {
		this.edition = edition;

		EndianDataInputStream is = new EndianDataInputStream(new ByteArrayInputStream(data));

		is.order(ByteOrder.LITTLE_ENDIAN);
		int version = is.readInt();// 4

		if (version > 73) {
			log("Version " + version + " is unknown. Loading it anyways...");
		}

		int crc32 = is.readInt() & 0xFF;// 4
		log("CRC32 " + crc32);

		log("Mobile world file version: " + version);

		int nameLength;

		if (version <= 49) {
			// TODO: Old string format
			nameLength = 0;
		} else {

			String worldName = "";

			nameLength = readString(worldName, is);

			log("World name is " + worldName);
		}

		if (version > 52)
			isCloudSave = is.readBoolean();// 1

		if (version == 53) {
			// TODO:
			@SuppressWarnings("unused")
			int time = is.read();
		}

		int timeStampsLen = 1;

		if (version > 53) {
			timeStampsLen = is.readInt();// 4

			int[] saveTimestamps = new int[timeStampsLen];

			log(timeStampsLen + " time stamps?");

			for (int i = 0; i < timeStampsLen; i++) {
				if (i == 0) {
					log("Time Stamps: ");
				} else {
					log(", ");
				}
				saveTimestamps[i] = (is.readInt());// (is.readInt() & 0xFF);
				log(saveTimestamps[i]);
			}
		}

		log();

		if (version > 65) {

			// Offset of where encryption starts in the file.
			int offset = 4 + 4 + 4 + (nameLength * 2) + 1 + (timeStampsLen * 4) + ((timeStampsLen % 2) == 0 ? 4 : 0);

			Blowfish bl = new Blowfish(Main.worldKey);
			bl.reinit();

			bl.decipher(data, offset, data, offset, data.length - offset);
		}

		worldId = is.readInt();

		rand = new Random(worldId);

		if (version > 47)
			creationTimestamp = is.readInt();

		rightWorld = is.readInt();

		bottomWorld = is.readShort();

		height = is.readShort();
		width = is.readShort();

		// what is this
//		for (int i = 0; i < 400; i++) {
//			is.readByte();
//		}

		for (WorldSize s : WorldSize.values()) {
			if (s.width == width && s.height == height) {
				size = s;
			}
		}

		if (size == null) {
			size = WorldSize.UNKNOWN;
		}

		log("World type/size is " + size);

		spawnX = is.readShort();
		spawnY = is.readShort();

		if (version <= 70)
			spawnY -= 1;

		groundLevel = is.readShort();
		rockLevel = is.readShort();

		if (version <= 66) {
			hellLevel = (short) (height - 190);
		} else {
			hellLevel = is.readShort();
		}

		time = is.readFloat();

		isDay = is.readBoolean();

		moonPhase = is.readByte();

		bloodMoon = is.readBoolean();

		days = is.readShort();

		if (version > 57) {
			isEclipse = is.readBoolean();
			moonType = is.readByte();
		} else {
			isEclipse = false;
			byte[] type = new byte[1];
			rand.nextBytes(type);
			moonType = type[0];
		}

		dungeonX = is.readShort();
		dungeonY = is.readShort();

		if (version > 57) {
			isCrimson = is.readBoolean();
			tempRaining = is.readBoolean();
			tempRainTime = is.readInt();
			tempMaxRain = is.readFloat();
			cloudBgActive = is.readFloat();
		} else {
			isCrimson = false;
			tempRaining = false;
			tempRainTime = 0;
			tempMaxRain = 0;

			int low = 8640;
			int high = 86400;
			cloudBgActive = -(rand.nextInt(high - low) + low);
		}

		eyeOfCthulhuDowned = is.readBoolean();
		eaterOfWorldsDowned = is.readBoolean();
		skeletronDowned = is.readBoolean();

		savedGoblin = is.readBoolean();
		savedWizard = is.readBoolean();
		savedMechanic = is.readBoolean();

		if (version > 60)
			savedStylist = is.readBoolean();

		if (version > 63)
			savedAngler = is.readBoolean();

		downedGoblins = is.readBoolean();
		downedClown = is.readBoolean();// "Clown down!" - A mad lad
		downedFrostLegion = is.readBoolean();

		if (version > 51)
			downedRabbit = is.readBoolean();

		if (version > 56)
			downedTurkor = is.readBoolean();

		if (version > 57) {
			downedQueenBee = is.readBoolean();
			downedDestroyer = is.readBoolean();
			downedTwins = is.readBoolean();
			downedSkeletronPrime = is.readBoolean();
			downedMechBossAny = is.readBoolean();
			downedPlantBoss = is.readBoolean();
			downedGolemBoss = is.readBoolean();
			downedPirates = is.readBoolean();
		}

		int killCountLen = 0;
		if (version > 67) {
			killCountLen += 1030;
			if (version > 69)
				killCountLen += 5;
		}

		killCount.clear();

		for (int i = 0; i < 1035; ++i) {
			if (i < killCountLen)
				killCount.add(is.readInt());
			else
				killCount.add(0);
		}

		shadowOrbSmashed = is.readBoolean();
		spawnMeteor = is.readBoolean();
		shadowOrbCount = is.readByte();

		altarCount = is.readInt();

		if (version > 57) {
			oreTier1 = is.readShort();
			oreTier2 = is.readShort();
			oreTier3 = is.readShort();
		} else {
			oreTier1 = 107;
			oreTier2 = 108;
			oreTier3 = 111;
		}

		hardMode = is.readBoolean();

		if (version > 55) {
			hardModeDayCount = is.readShort();
		}

		invasionDelay = is.readByte();
		invasionSize = is.readShort();
		invasionType = is.readByte();
		invasionX = is.readFloat();

		playersWhoCompletedQuestToday.clear();

		if (version > 63) {
			anglerQuest = is.readShort();
			questCompletedToday = is.readBoolean();
			int playersCompleted = is.readShort();
			for (int i = 0; i < playersCompleted; ++i) {
				String name = "";
				readString(name, is);
				playersWhoCompletedQuestToday.add(name);
			}
		}

		if (version > 57) {
			treeStyle0 = is.readByte();
			treeStyle1 = is.readByte();
			treeStyle2 = is.readByte();
			treeStyle3 = is.readByte();
			treeX0 = is.readShort();
			treeX1 = is.readShort(); // warning: always world end
			treeX2 = is.readShort(); // warning: always world end

			caveBackStyle0 = is.readByte();
			caveBackStyle1 = is.readByte();
			caveBackStyle2 = is.readByte();
			caveBackStyle3 = is.readByte();
			caveBackX0 = is.readShort();
			caveBackX1 = is.readShort(); // warning: TreeX1 is used instead
			caveBackX2 = is.readShort(); // warning: TreeX2 is used instead
			int styles = is.readByte();
			iceBackStyle = styles & 3;
			hellBackStyle = (styles >> 2) & 3;
			jungleBackStyle = (styles >> 4) & 3;

			bgTree = is.readByte();
			bgCorruption = is.readByte();
			bgJungle = is.readByte();
			bgSnow = is.readByte();
			bgHallow = is.readByte();
			bgCrimson = is.readByte();
			bgDesert = is.readByte();
			bgOcean = is.readByte();
		} else {
			caveBackX[0] = width / 2;
			caveBackX[1] = width;
			caveBackX[2] = width;
			caveBackStyle0 = 0;
			caveBackStyle1 = 1;
			caveBackStyle2 = 2;
			caveBackStyle3 = 3;
			iceBackStyle = 0;
			jungleBackStyle = 0;
			hellBackStyle = 0;
		}

		if (version > 61) {
			copper = is.readShort();
			iron = is.readShort();
			silver = is.readShort();
			gold = is.readShort();
		} else {
			copper = 7;
			iron = 6;
			silver = 9;
			gold = 8;
		}

		if (is.readInt() != 5678) {
			log("Failed to load basic world data!");
			return;
		}

//		int s = width * height;

		tiles = new Tile[width][height];

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				tiles[i][j] = new Tile();
			}
		}

		for (int x = 0; x < width; ++x) {
			// OnProgressChanged(null, new ProgressChangedEventArgs(x.ProgressPercentage(w.TilesWide), "Loading UndoTiles..."));

			for (int y = 0; y < height; y++) {
				Tile tile = ReadTileDataFromStreamMobile(is, version);

				// read complete, start compression
				tiles[x][y] = tile;

				int rle = is.readByte();
				if ((rle & 0x80) != 0) {
					rle = (rle & 0x7F) | (is.readByte() << 7);
				}

				if (rle > 0) {
					for (int k = y + 1; k < y + rle + 1; k++) {
						Tile tcopy = (Tile) tile.copy();
						tiles[x][k] = tcopy;
					}
					y = y + rle;
				}
			}
		}

		if (version > 68) {
			for (int x = 0; x < width; ++x) {
				for (int y = 0; y < height; ++y) {
					int mapVisibility = is.readByte() >> 4;
					tiles[x][y].mapVisibility = (byte) mapVisibility;

					int rle = is.readByte();
					if ((rle & 0x80) != 0) {
						rle = (rle & 0x7F) | (is.readByte() << 7);
					}

					if (rle > 0) {
						for (int k = y + 1; k < y + rle + 1; k++) {
							tiles[x][k].mapVisibility = (byte) mapVisibility;
						}
						y = y + rle;
					}
				}
			}
		}

		if (is.readInt() != 5678) {
			log("Failed to load tiles!");
			return;
		} else {
			log("Finished loading tiles.");
		}

		Imager.WorldToImage(this);
	}

	private Tile ReadTileDataFromStreamMobile(EndianDataInputStream is, int version) throws IOException {
		Tile tile = new Tile();

		int flags = 0;
		flags = is.readByte();

		tile.isActive = ((flags >> 0) & 1) != 0;

		if (version > 57) {
			tile.inActive = ((flags >> 1) & 1) != 0;
			tile.actuator = ((flags >> 7) & 1) != 0;
			boolean isHalfBrick = ((flags >> 2) & 1) != 0;
			int slope = ((flags >> 3) & 7);

			if (isHalfBrick) {
				switch (slope) {
				case 0:
					tile.brickStyle = BrickStyle.HalfBrick;
					break;
				case 1:
					tile.brickStyle = BrickStyle.SlopeTopLeftUp;
					break;
				case 2:
					tile.brickStyle = BrickStyle.SlopeBottomLeftUp;
					break;
				}
			} else {
				switch (slope) {
				case 0:
					tile.brickStyle = BrickStyle.Full;
					break;
				case 1:
					tile.brickStyle = BrickStyle.SlopeTopLeftDown;
					break;
				case 2:
					tile.brickStyle = BrickStyle.SlopeBottomLeftDown;
					break;
				}
			}
		}

		if (tile.isActive) {
			if (version > 58) {
				flags = is.readShort();
			} else {
				flags = is.readByte();
			}
			tile.type = (char) (flags & 0x1FF);

			if (version <= 57) {
				tile.type = Tile.UpdateTypeMobile(tile.type);
			}

			if (tile.type == 127)
				tile.isActive = false;

			int frameImportant = Tile.FrameImportantMobile(tile.type, version);

			if (frameImportant == 1) {
				tile.U = is.readShort();
				tile.V = is.readShort();

				if (tile.type == 144) // timer
					tile.V = 0;

				tile.UpdateTileFrameFromMobile();
			} else if (frameImportant == 2) {
				tile.U = 0;
				tile.V = 0;
			} else if (frameImportant == 0) {
				tile.U = -1;
				tile.V = -1;
			}

			if (version > 57) {
				tile.tileColor = is.readByte();
			}
		}

		tile.wall = is.readByte();
		if (tile.wall != 0 && version > 57)
			tile.wallColor = is.readByte();

		if (version > 50 && version <= 57) {
			int mapVisibility = is.readByte() >> 4;
			tile.mapVisibility = (byte) mapVisibility;
		}

		tile.liquidAmount = is.readByte();
		//log(Util.bytesToHex(new byte[] {tile.liquidAmount}));
		if (tile.liquidAmount != 0) {
			byte bite = is.readByte();

			int type = ((bite & 3) + 1);

			LiquidType liq = LiquidType.values()[type];
			
			tile.liquidType = liq;
		}
		flags = is.readByte();
		tile.wireRed = ((flags >> 4) & 1) != 0;
		if (version > 57) {
			tile.wireGreen = ((flags >> 5) & 1) != 0;
			tile.wireBlue = ((flags >> 6) & 1) != 0;
		}

		if (version > 59 && version <= 68) {
			int mapVisibility = is.readByte() >> 4;
			tile.mapVisibility = (byte) mapVisibility;
		}

		return tile;
	}

	private int readString(String worldName, EndianDataInputStream is) throws IOException {
		int nameLength = is.readInt();// 4

		byte[] nameBytes = new byte[nameLength];

		for (int i = 0; i < nameLength; i++) {// nameLength * 2
			nameBytes[i] = is.readByte();
			is.readByte();
		}

		worldName = new String(nameBytes, "UTF-8");
		return nameLength;
	}
}
